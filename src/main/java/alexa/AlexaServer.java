package alexa;

import alexa.skills.*;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by amen on 9/14/17.
 */

// zakomentuj pierwszy średnik poniższej linii
/* 1)
public class alexa.AlexaServer {

    public final static alexa.AlexaServer instance = new alexa.AlexaServer();
    private alexa.AlexaServer(){};
// */
// odkomentuj pierwszy średnik poniższej linii
//* 2)
public class AlexaServer implements Observer {

    public AlexaServer() {
    }

    public void update(Observable o, Object arg) {
        if (arg instanceof Request) {
            Request request = (Request) arg;

            AbstractSkill callback = parseRequest(request);
            request.getDeviceToCall().invoke(callback);
        }
    }
    // */

    public AbstractSkill parseRequest(Request request) {
        String req = request.getRequest();
        req = req.toLowerCase();
        if (req.startsWith("alexa")) {
            req = req.substring(6); // 5 znaków oraz spacja.
            if (req.toLowerCase().startsWith("add todo task")) {
                req = req.substring(14); // ucinam 13 znaków "add todo task" oraz spację za nią.

                String task = req;//zostaje mi tylko zadanie.
                return new AddToDoTaskSkill(task);
            }
            if (req.toLowerCase().startsWith("remove todo task")) {
                req = req.substring(17); // ucinam 17 znaków "remove todo task" oraz spację za nią.

                String task = req;//zostaje mi tylko zadanie.
                return new RemoveToDoTaskSkill(task);
            }
            if (req.toLowerCase().startsWith("list tasks")) {
                return new ListToDoTaskSkill();
            }
            if (req.toLowerCase().startsWith("what's the time") ||
                    req.toLowerCase().startsWith("give me the time") ||
                    req.toLowerCase().startsWith("the time")) {
                return new DateTimeSkill();
            }
        }
        return new DontKnowSkill();
    }
}
