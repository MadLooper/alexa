package alexa;

import alexa.skills.AbstractSkill;

import java.util.Observable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by amen on 9/14/17.
 */

//public class alexa.AlexaDevice /*implements Observer*/{
public class AlexaDevice extends Observable /*implements Observer*/ {

    private static int alexaIds = 0;
    private int id = alexaIds;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    private AlexaAccountInfo info = new AlexaAccountInfo();

    public AlexaDevice() {
    }

    /**
     * Nie wiesz jeszcze jaki to request.
     *
     * @param requestString - zapytanie (słowa) od wywołującego.
     */
    public void sendRequest(String requestString) {
        if (countObservers() == 0) {
            System.out.println("Sorry, i'm having problem connecting to the internet.");
            return;
        }

        if (requestString.toLowerCase().startsWith("alexa ")) {
            Request request = new Request(requestString, this);
            // 1)
            // informujemy serwer o nowym requeście
            //alexa.AlexaServer.instance.parseRequest(request);

            // 2)
            setChanged();
            notifyObservers(request);
        }
    }

    public void invoke(AbstractSkill callback) {
//        callback.addObserver(this);
        callback.invokeUsing(info);
        executorService.submit(callback);
    }
//    @Override
//    public void update(Observable o, Object arg) {
//        // todo: on skill finished
//    }
}
