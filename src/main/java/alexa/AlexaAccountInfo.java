package alexa;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by amen on 9/14/17.
 */
public class AlexaAccountInfo {
    // Może być wykonane jako singleton (dla uproszczenia)
    private List<String> tasks;

    public AlexaAccountInfo() {
        this.tasks = new ArrayList<>();
    }

    public List<String> getTasks() {
        return tasks;
    }

    public void setTasks(List<String> tasks) {
        this.tasks = tasks;
    }
}
